package com.utesurdako.todo.Data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.utesurdako.todo.Fragments.List.ListFragmentDirections
import com.utesurdako.todo.Model.Note
import com.utesurdako.todo.R
import com.utesurdako.todo.databinding.NoteItemBinding
import java.util.*

class NoteAdapter : RecyclerView.Adapter<NoteAdapter.NoteHolder>() {
    val noteList = ArrayList<Note>()

    class NoteHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = NoteItemBinding.bind(item)

        fun bind(note: Note) = with(binding) {
            imageView.setImageResource(note.imageId)
            txtTitle.text = note.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        return NoteHolder(view)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val currentNote = noteList[position]

        holder.bind(currentNote)
        holder.binding.gridLayout.setOnClickListener{
            val action = ListFragmentDirections.actionListFragmentToInfoFragment(currentNote)
            holder.itemView.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    fun addNotes(notes: List<Note>)
    {
        noteList.clear()
        noteList.addAll(notes)
        notifyDataSetChanged()
    }
}