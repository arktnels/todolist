package com.utesurdako.todo.Fragments.Add

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.utesurdako.todo.Model.Note
import com.utesurdako.todo.ViewModel.NoteViewModel
import com.utesurdako.todo.R
import com.utesurdako.todo.databinding.FragmentNewNoteBinding

class FragmentNewNote : Fragment() {
    lateinit var binding: FragmentNewNoteBinding
    private lateinit var noteViewModel: NoteViewModel

    private val imageIdList = arrayOf(
        R.drawable.note1,
        R.drawable.note2,
        R.drawable.note3,
        R.drawable.note4,
        R.drawable.note5
    )
    private var imageId = R.drawable.note1
    private var indexImage = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewNoteBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        btNext.setOnClickListener{
            indexImage++
            if (indexImage >= imageIdList.size)
                indexImage = 0
            imageId = imageIdList[indexImage]
            iconNote.setImageResource(imageId)
        }
        noteViewModel = ViewModelProvider(this@FragmentNewNote).get(NoteViewModel::class.java)
        btDone.setOnClickListener{
            insertDataToDatabase()
        }
    }

    private fun insertDataToDatabase(){
        val imageId = imageId
        val title = binding.inpfldTitle.text.toString()
        val description = binding.inpfldDescr.text.toString()

        if (inputCheck(title, description))
        {
            val note = Note(0, imageId, title, description)
            noteViewModel.addNote(note)
            findNavController().navigate(R.id.action_fragmentNewNote_to_listFragment)
        }
        else
        {
            Toast.makeText(requireContext(), "Please fill out all fields", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(title: String, description: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(description))
    }

    companion object {
        @JvmStatic
        fun newInstance() = FragmentNewNote()
    }
}