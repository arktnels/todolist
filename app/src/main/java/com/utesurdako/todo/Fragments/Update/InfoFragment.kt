package com.utesurdako.todo.Fragments.Update

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.utesurdako.todo.Model.Note
import com.utesurdako.todo.R
import com.utesurdako.todo.ViewModel.NoteViewModel
import com.utesurdako.todo.databinding.FragmentInfoBinding

class InfoFragment : Fragment() {
    lateinit var binding: FragmentInfoBinding
    private val args by navArgs<InfoFragmentArgs>()
    private lateinit var noteViewModel: NoteViewModel

    private val imageIdList = arrayOf(
        R.drawable.note1,
        R.drawable.note2,
        R.drawable.note3,
        R.drawable.note4,
        R.drawable.note5
    )
    private var imageId = R.drawable.note1
    private var indexImage = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentInfoBinding.inflate(inflater)

        noteViewModel = ViewModelProvider(this@InfoFragment).get(NoteViewModel::class.java)

        binding.iconNoteUpdate.setImageResource(args.currentNote.imageId)
        binding.inpfldTitle.setText(args.currentNote.title)
        binding.inpfldDescr.setText(args.currentNote.description)

        imageId = args.currentNote.imageId

        for (index in 0..(imageIdList.size - 1)){
            if (imageIdList[index].equals(binding.iconNoteUpdate)){
                indexImage = index
                break
            }
        }

        binding.btNext.setOnClickListener{
            indexImage++
            if (indexImage >= imageIdList.size)
                indexImage = 0
            imageId = imageIdList[indexImage]
            binding.iconNoteUpdate.setImageResource(imageId)
        }

        binding.btDone.setOnClickListener{
            updateNote()
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId ==R.id.menu_delete)
        {
            deleteUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateNote(){
        val imageId = imageId
        val title = binding.inpfldTitle.text.toString()
        val description = binding.inpfldDescr.text.toString()

        if (inputCheck(title, description))
        {
            val note = Note(args.currentNote.id, imageId, title, description)
            noteViewModel.updateNote(note)
            findNavController().navigate(R.id.action_infoFragment_to_listFragment)
        }
    }

    private fun inputCheck(title: String, description: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(description))
    }

    private fun deleteUser(){
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(R.string.yes) {_, _ ->
            noteViewModel.deleteAllNotes(args.currentNote)
            Toast.makeText(
                requireContext(),
                "${getString(R.string.successfully_removed)}: ${args.currentNote.title}",
                Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_infoFragment_to_listFragment)
        }
        builder.setNegativeButton(R.string.no){_, _ ->}
        builder.setTitle("${getString(R.string.delete)} ${args.currentNote.title}")
        builder.setMessage("${getString(R.string.are_you_sure_delete)} ${args.currentNote.title}")
        builder.create().show()
    }
}