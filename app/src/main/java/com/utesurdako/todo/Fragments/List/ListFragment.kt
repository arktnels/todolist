package com.utesurdako.todo.Fragments.List

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.utesurdako.todo.Data.NoteAdapter
import com.utesurdako.todo.ViewModel.NoteViewModel
import com.utesurdako.todo.R
import com.utesurdako.todo.databinding.FragmentListBinding

class ListFragment : Fragment() {
    lateinit var binding: FragmentListBinding
    private lateinit var noteViewModel: NoteViewModel
    private val adapter = NoteAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentListBinding.inflate(inflater)

        binding.apply {
            rcView.layoutManager = GridLayoutManager(requireContext(), 2)
            rcView.adapter = adapter

            noteViewModel = ViewModelProvider(this@ListFragment).get((NoteViewModel::class.java))
            noteViewModel.readAllData.observe(viewLifecycleOwner, Observer { notes ->
                adapter.addNotes(notes)
            })

            btAddNote.setOnClickListener{
                findNavController().navigate(R.id.action_listFragment_to_fragmentNewNote)
            }
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete){
            deleteAllNotes()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllNotes(){
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(R.string.yes) {_, _ ->
            noteViewModel.deleteAllNotes()
            Toast.makeText(
                requireContext(),
                "${getString(R.string.successfully_all_removed)}",
                Toast.LENGTH_SHORT).show()
        }
        builder.setNegativeButton(R.string.no){_, _ ->}
        builder.setTitle("${getString(R.string.deletete_all)}?")
        builder.setMessage("${getString(R.string.are_you_sure_all_delete)}?")
        builder.create().show()
    }

    companion object {
        fun newInstance(param1: String, param2: String) = ListFragment()
    }
}